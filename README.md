# Docker ELK

Docker compose file to ELK stack

## Getting Started

Instructions to run a ELK with four commands.

### Prerequisites

* [Docker](https://docs.docker.com/engine/installation/linux/docker-ce/debian/) - Used to run container
* [Docker Compose](https://docs.docker.com/compose/install/) - Used to provision all containers 


### Run ELK

Clone Repo

```
git clone https://bitbucket.org/correiabrux/elk.git
```

Go to the new path

```
$ cd elk/
```


Run stack

```
$ sudo docker-compose up -d
```

### Load log files

Copy files *.log to path /tmp in docker server.


### Access Kibana

* [Kibana](http://127.0.0.1:5601) - Exposed port 5601 

## Authors

* **Bruno Correia** - *Initial work* - [DevopsClub](http://www.devopsclub.com.br)
